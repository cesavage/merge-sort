import unittest
from mergesort.modules.recursion_display_indentor import RecursionDisplayIndentor as Indentor

class test_indentor(unittest.TestCase):
    def test_printIndent(self):
        displayIndentor = Indentor()
        indentString = displayIndentor.printIndent()
        self.assertEquals(indentString, "")

    def test_growIndent(self):
        displayIndentor = Indentor()
        displayIndentor.growIndent()
        indentString = displayIndentor.printIndent()
        self.assertEquals(indentString, "  ")

    def test_shrinkIndent(self):
        displayIndentor = Indentor()
        displayIndentor.growIndent()
        displayIndentor.growIndent()
        displayIndentor.shrinkIndent()
        indentString = displayIndentor.printIndent()
        self.assertEquals(indentString, "  ")