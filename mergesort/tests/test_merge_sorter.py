import unittest
#import mergesort.modules.merge_sorter as MergeSorter
from mergesort.modules.merge_sorter import Sorter as MergeSorter

class testMergeSorter(unittest.TestCase):
    def test_sortWithStatusOutput(self):
        integerList = [5,4,3,1,2]

        mergeSorter = MergeSorter()

        result = mergeSorter.sortWithStatusOutput(integerList)
        self.assertEqual(result,[1,2,3,4,5])