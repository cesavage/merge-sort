import mergesort.modules.recursion_display_indentor as Indenter



class Sorter:
    """
    Provides functionality to implement the merge sort algorithm.
    """

    indent = Indenter.RecursionDisplayIndentor()

    @staticmethod
    def sortWithStatusOutput(userInput): 
        """
        Performs execution of the merge-sort algorithm.

        :param name: sourceArray -- The array of integers to be sorted.
        :param type: list

        :returns: list -- The sorted list of integers.
        """  
        return None

    def __init__(self):

        
        def _definePrivateAttributesAndMethods(self):

            def _createSubArray(sourceArray, startIndex, endIndex):
                """
                Private method that creates a subarray of sourceArray that consists of
                sourceArray[startIndex] - sourceArray[endIndex]

                :param name: sourceArray -- the array from which a subarray should be created.
                :param type: list

                :param name: startIndex -- sourceArray index whose value is used as the first value of the subarray.
                :param type: int

                :param name: endIndex -- sourceArray index whose value is used as the last value of the subarray.
                :param type: int

                :returns: list -- the subarray created.

                """
                subArray = []
                for arrayIndex in range(startIndex, endIndex ):
                    subArray.append(sourceArray[arrayIndex])

                return subArray



            def _partitionArray(sourceArray):
                """
                Private method that finds the midpoint of an array and
                creates two subarrays representing the array split at the 
                midpoint.

                :param name: sourceArray
                :param type: list

                :returns: dict -- a dictionary containing the left and right array partitions.
                """
                leftPartition = []
                rightPartition = []
                sourceArrayMidpoint = int( len( sourceArray ) / 2 )

                leftPartition = _createSubArray(sourceArray, 0, sourceArrayMidpoint)
                rightPartition = _createSubArray(sourceArray, sourceArrayMidpoint, len(sourceArray))

                arrayPartitions = {"leftPartition" : leftPartition, "rightPartition" : rightPartition }

                return arrayPartitions



            def _mergeArrays(leftArray, rightArray, sourceArray):
                """
                Private method that performs the 'sort' step of the mergesort algorithm by combining
                two lists back into their source list in ascending, numerical order.

                :param name: leftArray -- One of two arrays to be merged.
                :param type: list

                :param name: rightArray -- The other of two arrays to be merged.
                :param type: list

                :param name: sourceArray -- The array storing the result of the merged arrays.
                :param type: list
                """
                leftArrayCurrentIndex = 0
                rightArrayCurrentIndex = 0
                sourceArrayCurrentIndex = 0

                self.indent.shrinkIndent()

                print("\n" + self.indent.printIndent() + "[]+[] Merging arrays " + str(leftArray) + " and " + str(rightArray) )

                while leftArrayCurrentIndex in range( 0, len(leftArray) ) and  rightArrayCurrentIndex in range(0, len(rightArray) ):
                    if leftArray[leftArrayCurrentIndex] <= rightArray[rightArrayCurrentIndex]:
                        sourceArray[sourceArrayCurrentIndex] = leftArray[leftArrayCurrentIndex]
                        leftArrayCurrentIndex += 1
                        sourceArrayCurrentIndex += 1
                    
                    else:
                        sourceArray[sourceArrayCurrentIndex] = rightArray[rightArrayCurrentIndex]
                        rightArrayCurrentIndex += 1
                        sourceArrayCurrentIndex += 1

                while leftArrayCurrentIndex >= len(leftArray) and rightArrayCurrentIndex < len(rightArray):
                    sourceArray[sourceArrayCurrentIndex] = rightArray[rightArrayCurrentIndex]
                    rightArrayCurrentIndex += 1
                    sourceArrayCurrentIndex += 1

                while rightArrayCurrentIndex >= len(rightArray) and leftArrayCurrentIndex < len(leftArray):
                    sourceArray[sourceArrayCurrentIndex] = leftArray[leftArrayCurrentIndex]
                    leftArrayCurrentIndex += 1
                    sourceArrayCurrentIndex += 1

                print(self.indent.printIndent() + "Result: " + str(sourceArray))



            def sortWithStatusOutput(sourceArray):  
                """
                Private method that performs execution of the merge-sort algorithm.
                Is used as the definition of this class's public sortWithStatusOutput() method.

                :param name: sourceArray -- The array of integers to be sorted.
                :param type: list

                :returns: list -- The sorted list of integers.
                """  
                if len(sourceArray) > 1:
                    
                    arrayPartitions = _partitionArray(sourceArray)
                    leftPartition = arrayPartitions["leftPartition"]
                    rightPartition = arrayPartitions["rightPartition"]
                    
                    print(self.indent.printIndent() + "Result: " + str(leftPartition) + " " + str(rightPartition) )
                    self.indent.growIndent()
                    
                    print("\n" + self.indent.printIndent() + "[]/[] Partitioning left array " + str(leftPartition) )
                    sortWithStatusOutput(leftPartition)

                    print("\n" + self.indent.printIndent() + "[]/[] Partitioning right array " + str(rightPartition) )
                    sortWithStatusOutput(rightPartition)

                    _mergeArrays(leftPartition, rightPartition, sourceArray)
                return sourceArray

            self.sortWithStatusOutput = sortWithStatusOutput

        _definePrivateAttributesAndMethods(self)