
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <title>Concepts demonstrated &#8212; Merge Sort 1.0 documentation</title>
    <link rel="stylesheet" href="_static/classic.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/language_data.js"></script>
    
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="prev" title="The Merge Sort Algorithm" href="algorithm.html" /> 
  </head><body>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="algorithm.html" title="The Merge Sort Algorithm"
             accesskey="P">previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">Merge Sort 1.0 documentation</a> &#187;</li> 
      </ul>
    </div>  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="concepts-demonstrated">
<h1>Concepts demonstrated<a class="headerlink" href="#concepts-demonstrated" title="Permalink to this headline">¶</a></h1>
<p>This implementation highlights an understanding of several important software development concepts.</p>
<div class="section" id="the-python-programming-language">
<h2>The Python programming language<a class="headerlink" href="#the-python-programming-language" title="Permalink to this headline">¶</a></h2>
<p>The Python programming language is a strongly, dynamically typed language. This means that the Python interpreter can infer the types of objects without those types being explicitly declared by the developer. However, because Python is also strongly-typed, the interpreter refrains from performing undirected type coercions, relying on the developer to cast objects to different types, as needed.</p>
<p>The Python language is widely-considered to be an interpreted language, though its implementation relies on a combination of compilation and interpretation. Python code is most often compiled into an intermediate code, often referred to as <em>bytecode</em>, which is then interpreted by the Python interpreter at runtime, allowing the code to be optimized at runtime for the hardware stack on which it is being executed.</p>
</div>
<div class="section" id="principles-of-clean-code">
<h2>Principles of clean code<a class="headerlink" href="#principles-of-clean-code" title="Permalink to this headline">¶</a></h2>
<p>Ian Sommerville’s <em>Software Engineering</em>, published in 2010, estimates that software maintenance costs can be as high as two to four times as much as initial development costs. In other words, more time and resources are spent by developers in existing codebases than is spent in writing new ones.</p>
<p>Thoughtfully-written programs are easier for humans to read, understand, and test, improving the software maintenance experience for developers and delivering better software to clients. Principles of clean code are numerous, and specifics are sometimes debatable, but at a high level, clean code is consistently and logically structured, separated into manageable pieces, and relies not on comments, but on the on clear and consistent use of method names, variable names, and control structures to indicate a program’s logic.</p>
<p>Clean code requires not only an understanding of a problem domain, approaches to solving the problem, and a grasp of the language’s features, but also requires a thoughtful analysis of the code being written. Just as a book written in a human-readable language undergoes proofreading and revisions to improve the quality of writing and readability of the finished piece, so does a program that is written according to some established clean code standards.</p>
</div>
<div class="section" id="encapsulation">
<h2>Encapsulation<a class="headerlink" href="#encapsulation" title="Permalink to this headline">¶</a></h2>
<p>The idea of encapsulation is at the heart of the object-oriented programming paradigm, where <em>objects</em> created by developers can hide away the sometimes-complex implementation of their features, providing APIs for other developers to use.</p>
<p>Classes and interfaces can each be used to define an object’s structure, while classes most often contain the specific logic executed by an object’s methods. By encapsulating complex functionality within an object, a developer can help protect important program logic from inadvertent manipulation, and provide easier-to-use tools for other developers who needn’t worry about how an object’s functionality was implemented. At the same time, should an aspect of an object’s logic need to be adjusted, that logic can be adjusted independently of the API used to access it, improving software maintainability.</p>
</div>
<div class="section" id="closures-as-a-means-for-encapsulation">
<h2>Closures as a means for encapsulation<a class="headerlink" href="#closures-as-a-means-for-encapsulation" title="Permalink to this headline">¶</a></h2>
<p>Some languages provide robust toolsets for creating and maintaining encapsulated objects. Python and JavaScript are two object-oriented languages that don’t provide implementations of the “private” keyword, which is often heavily relied-upon to provide encapsulation in other languages. In languages where functions are also considered objects, closures can be leveraged to provide encapsulated access to attributes through carefully constructed methods.</p>
<p>A closure occurs as the result of nested functions. When one function declares an attribute, and a nested function accesses that attribute, the attribute in question is unable to be accessed outside of it’s parent function, but can be accessed by a function that is nested within that parent.</p>
<p>Assigning that nested function to a variable preserves the nested function’s access to the otherwise inaccessible variable, allowing the developer to provide access to the variable only through the methods that he or she creates.</p>
</div>
<div class="section" id="recursion-as-a-program-control-structure">
<h2>Recursion as a program-control structure<a class="headerlink" href="#recursion-as-a-program-control-structure" title="Permalink to this headline">¶</a></h2>
<p>Recursion, which most commonly occurs when a method calls itself, allows developers to maximize code efficiency by reusing blocks of logic. When the same logic needs to be applied to a dataset repeatedly, recursion may be a good choice for controlling program flow.</p>
<p>Problems that exhibit optimal substructure, are often good candidates for recursive solutions. In recursion, a known truth is established called the <em>base case</em>. The recursive program continues to decompose the problem at hand until it reaches the base case, at which point the recursion stops.</p>
<p>Any program that can be written recursively can also be written iteratively, and vice versa. While the resulting code base of a recursive implementation is often smaller, some developers find recursive code difficult to read and interpret.</p>
</div>
<div class="section" id="use-of-version-control-git">
<h2>Use of version control (Git)<a class="headerlink" href="#use-of-version-control-git" title="Permalink to this headline">¶</a></h2>
<p>Some part of software development requires experimentation with code, teamwork, and perspective. Even in a sandbox environment, experimentation comes with risks, not the least of which is the risk of losing a codebase that exists in a known, predictable state, even if that state does not represent properly-functioning code.</p>
<p>Version control systems provide a tool to facilitate experimentation, sharing, and historical perspective of codebases. Using a version control system allows developers to experiment more freely, knowing that a historical snapshot of a past configuration can be restored, provides robust mechanisms for sharing code and contributing to projects in a team setting, and keeps track of all changes that have been made to a code base over time.</p>
</div>
<div class="section" id="singleton-design-pattern">
<h2>Singleton design pattern<a class="headerlink" href="#singleton-design-pattern" title="Permalink to this headline">¶</a></h2>
<p>A singleton is an object that policies its own instantiation to ensure that only one instance of itself is created over the lifetime of a program, or at least within a specific scope, and can provide global visibility of an encapsulated object’s attributes and methods while reducing the impact of problems that can arise from global access.</p>
</div>
<div class="section" id="regular-expressions">
<h2>Regular expressions<a class="headerlink" href="#regular-expressions" title="Permalink to this headline">¶</a></h2>
<p>Regular expressions are perhaps the most powerful and flexible tool available to developers when analyzing string values. Allowing a developer to target specific patterns of characters, regular expressions or <em>regex</em> can be used in a variety of string evaluations and manipulations.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Concepts demonstrated</a><ul>
<li><a class="reference internal" href="#the-python-programming-language">The Python programming language</a></li>
<li><a class="reference internal" href="#principles-of-clean-code">Principles of clean code</a></li>
<li><a class="reference internal" href="#encapsulation">Encapsulation</a></li>
<li><a class="reference internal" href="#closures-as-a-means-for-encapsulation">Closures as a means for encapsulation</a></li>
<li><a class="reference internal" href="#recursion-as-a-program-control-structure">Recursion as a program-control structure</a></li>
<li><a class="reference internal" href="#use-of-version-control-git">Use of version control (Git)</a></li>
<li><a class="reference internal" href="#singleton-design-pattern">Singleton design pattern</a></li>
<li><a class="reference internal" href="#regular-expressions">Regular expressions</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="algorithm.html"
                        title="previous chapter">The Merge Sort Algorithm</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/concepts.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="py-modindex.html" title="Python Module Index"
             >modules</a> |</li>
        <li class="right" >
          <a href="algorithm.html" title="The Merge Sort Algorithm"
             >previous</a> |</li>
        <li class="nav-item nav-item-0"><a href="index.html">Merge Sort 1.0 documentation</a> &#187;</li> 
      </ul>
    </div>
    <div class="footer" role="contentinfo">
        &#169; Copyright 2019, Chris Savage.
      Created using <a href="http://sphinx-doc.org/">Sphinx</a> 2.3.1.
    </div>
  </body>
</html>