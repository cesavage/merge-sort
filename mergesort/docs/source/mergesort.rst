mergesort package
=================

Subpackages
-----------

.. toctree::

   mergesort.modules

Submodules
----------

mergesort.mergesort module
--------------------------

.. automodule:: mergesort.mergesort
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: mergesort
   :members:
   :undoc-members:
   :show-inheritance:
