The Merge Sort Algorithm
========================

The Merge Sort algorithm is a sorting algorithm that leverages the optimal substructure of a sorting problem.

What is optimal substructure?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In algorithmic thinking, a problem presents optimal substructure if it can be decomposed into smaller, solvable, problems *and* the solutions to those smaller problems can be combined to create a solution for the actual problem.

How does optimal substructure pertain to the merge-sort algorithm?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The merge sort algorithm relies on the idea that a list of one item is, by its essence, sorted. 

The algorithm sorts by beginning with a group of objects (in this case, a list of integers) and breaking that group into two equally-sized groups, a process known as *partitioning*. Each of these new, smaller groups are then broken in half, or *partitioned*, repeatedly until the program is left with groups of only one item each. Each of these single-item groups are, by their essence, sorted.

Now, the program looks at two of these one-item groups simultaneously, chooses the smallest value between the two, and places that value into the first element of a new group. (Technically-speaking, the two items are placed back into the group from which they were originally partitioned, to conserve memory.) The remaining value, being the greater of the two values, is put into the second element of the new group. At this point, this new group of two items is in sorted order. This process of combining the two groups is called *merging* and represents the optimal substructure of the sorting problem. By continuing to sort and combine these smaller groups, all numbers in the original group are ultimately sorted.

The program continues this path of execution, by next comparing two (2) two-item groups, which are in sorted order themselves, looking at the first element of each group (the smallest element in each) and choosing the smallest element to place into a new group. Ultimately, the program is left with two groups that represent two halves of the originally-provided group. When the process of merging these groups is complete, all values from the original group are represented in sorted order in one group.

