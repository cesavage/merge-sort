.. Merge Sort documentation master file, created by
   sphinx-quickstart on Mon Dec 23 18:20:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Merge Sort
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   algorithm
   concepts

* :ref:`modindex`
* :ref:`genindex`