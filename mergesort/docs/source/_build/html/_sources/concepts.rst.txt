Concepts demonstrated
=====================
This implementation highlights an understanding of several important software development concepts.

The Python programming langugage
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The Python programming language is a strongly, dynamically typed language. This means that the Python interpreter can infer the types of objects without those types being explicitly-declared by the developer. However, becuase Python is also strongly-typed, the interpreter refrains from performing undirected type coersions, relying on the developer to cast objects to different types, as needed.

The Python language is considered to be an interpreted langugage, though its implementation relies on a combination of compilation and interpretation. Python code is most often compiled into an intermediate code, often referred to as *bytecode*, which is then interpreted by the Python interpretor at runtime, allowing the code to be optimized at runtime for the hardware stack on which it is being executed.

Principles of clean code
^^^^^^^^^^^^^^^^^^^^^^^^
Ian Sommerville's *Software Engineering*, published in 2010, estimates that software maintenance costs can be as high as two to four times as much as initial development costs. In other words, more time and resources are spent by developers in existing codebases than is spent in writing new ones.

Thoughtfully-written programs are easier for humans to read, understand, and test, improving the software maintenance experience for developers and delivering better software to clients. Principles of clean code are numerous, and specifics are sometimes debatable, but at a high level, clean code is consistenly and logically structured, separated into manageable pieces, and relies not on comments, but on the on clear and consistent use of method names, variable names, and control structures to indicate a program's logic.

Clean code requires not only an understanding of a problem domain, approaches to solving the problem, and a grasp of the language's features in use, but also requires a thoughtful analysis of the code being written. Just as a book written in a human-readable langugage undergoes proofreading and revisions to improve the quality of the written word, so does a program that is written according to some established clean code standards.

Encapsulation
^^^^^^^^^^^^^
The idea of encapsulation is at the heart of the object-oriented programming paradigm, where *objects* created by developers can hide away the sometimes-complex implementation of their features, providing APIs for other developers to use.

Classes and interfaces can each be used to define an object's structure, while classes most often contain the specific logic executed by an object's methods. By encapsulating complex functionality within an object, a developer can help protect important program logic from inadvertent manipulation, and provide easier-to-use tools for other developers who needn't worry about how an object's functionality was implemented. At the same time, should an aspect of an object's logic need to be adjusted, that logic can be adjusted independently of the API used to access it, improving software maintainability.

Closures as a means for encapsulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some languages do provide robust toolsets for creating and maintaining encapsulated objects. Python and JavaScript are two object-oriented languages that to not provide implementations of the "private" keyword, which is often heavily relied-upon to provide encapsulation. In languages where functions are also considered objects, closures can be leveraged to provide encapsulated access to attributes through carefully constructed methods. 

A closure occurs as the result of nested functions. When one function declares an attribute, and a nested function accesses that attribute, the attribute in question is unable to be accessed outside of it's parent function, but can be accessed by a function that is nested within that parent.

Assigning that nested function to a variable preserves the nested function's access to the otherwise inaccessible variable, allowing the developer to provide access to the variable only through the methods that he or she creates.

Recursion as a program-control structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Recursion, which most commonly occurs when a method calls itself, allows developers to maximize code efficiency by reusing blocks of logic. When the same logic needs to be applied to a dataset repeatedly, recursion may be a good choice for controlling program flow.

Problems that exhibit optimal subsstructure, are often good candidates for recursive solution. In recursion, a known truth is established called the *base case*. The recursive program continues to decompose the problem at hand until it reaches the base case, at which point the recursion stops. 

Any program that can be written recursively can also be written iteratively, and vice versa. While the resulting code base of a recursive implementation is often smaller, some developers find recursive code difficult to read and interpret.

Use of version control (Git)
^^^^^^^^^^^^^^^^^^^^^^
Some part of software development requires experimentation with code, teamwork, and perspective. Even in a sandbox environment, experimentation comes with risks, not the least of which is the risk of losing a codebase that exists in a known, predictable state, even if that state does not represent properly-functioning code.

Version control systems provide a tool to facilitate experimentation, sharing, and historical perspective of codebases. Using a version control system allows developers to experiment more freely, knowing that a historical snapshot of a past configuration can be restored, provides robust mechanisms for sharing code and contributing to projects in a team setting, and keeps track of all changes that have been made to a code base over time.

Singleton design pattern
^^^^^^^^^^^^^^^^^^^^^^^^
A singleton is an object that policies its own instantiation to ensure that only one instance of itself is created over the lifteime of a program, or at least within a specific scope, and can provide global visibility of an encapsulated object's attributes and methods while reducing the impact of problems that can arise from global access.

Regular expressions
^^^^^^^^^^^^^^^^^^^
Regular expressions are perhaps the most powerful and flexible tool available to developers when working with string values. Allowing a developer to target specific patterns of characters, regular expressions or *regex* can be used in a variety of string evaluations and manipulations including search and data-validation, 





